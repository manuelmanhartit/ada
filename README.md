![picture](logo.png)

# Ada 3.0 (Alpha)

This is my php website creation support tool ADA (Alpha Dust Advanced) - it is built with PHP.

The first design of the framework was based on a hexo theme called Alpha Dust created by Jonathan Klughertz. Since it did the job very well but ultimately was not flexible enough I kept the name and did a full rewrite.

This worked really well for some years, but now we have new requirements like to fetch data from a headless CMS. So I did a bigger refactoring and build a connector to fetch all the data from Strapi CMS instead of loading it from (config) files.

My next step is to move even the partials (HTML / JS / CSS) into strapi to be able to create a website without any development - everything can be done via strapi - the file based approach will still be supported - maybe we will even allow some kind of mixed mode, where you can do stuff in files and other stuff in strapi.

Also the contents should be cacheable so that when strapi is slow / not available the contents can still be accessed.

So now you can use ADA to easily create either static or dynamic (PHP) webpages in your own design. In this README you will find a tutorial for both ways and adapting everything to your needs.

This project is used for the current website of 'Manuel Manhart IT'.

## TLDR or Gimme, gimme, gimme a (static) website within the next five minutes...

1. Just checkout (and start) [ada-example-01-static](https://bitbucket.org/manuelmanhartit/ada-example-01-static) for a quickstart. You can also check the README of that example project to learn how to create that from scratch by yourself.
2. Place it in your php development environment.
2. Startup your php development environment.
3. Call the website in your browser.

Wow, you have created a website, and it was really fast too.

For the code of the full example have a look at `examples/static`.

If you do not like the default paths, you can easily change them if you have a look in the 'Configuration' section.

## List of examples

1. Build a [static website](https://bitbucket.org/manuelmanhartit/ada-example-01-static)
2. Build a [PHP WebApp](https://bitbucket.org/manuelmanhartit/ada-example-02-dynamic)
3. Build a [PHP WebApp with some static pages](https://bitbucket.org/manuelmanhartit/ada-example-03-mixed)

## Getting started or Tell me more, tell me more...

So you want to either to learn more about the framework or have more advanced use cases - well lets get started.

As already mentioned, there are two ways to use this framework. To display static html pages where parts are reused, eg. Header, Navigation, Footer and only the Content part really differs. (see also previous TLDR section)

But as soon as you want to change the configuration or even use your own php scripts in combination of ADA you will need some of the knowledge in the next sections.

### Project structure

The (default) project file structure is:

	/                 ... project dir, will be accessed by the webserver
	/.ada-config.json ... (optional) configuration of ADA framework
	/ada              ... the ada framework itself (we are here ATM, should be checked out as submodule in your project)
		/ .ada-config-default.json ... default configuration of ADA framework
		/ framework.php            ... the code of ADA framework
		/ logo.png                 ... The logo of ADA framework (see Special thanks section)
		/ examples                 ... some examples use cases for easy learning how to use the framework
		/ twig-loader.php          ... A loader to load the templating engine without compose (see Special thanks section below)
		/ Twig                     ... The templating engine (see Build with section below)

The (default) workspace file structure is:

	/ada-files        ... the files used for templating (default workspace dir)
		/ template.html    ... the template file where the content files will be rendered in
		/ site-config.json ... the configuration file for the site (will be used in template.html heavily and available in all templates)
		/ content          ... all html templates (rendered in the content body section)
		/ css              ... all css files rendered in the finished page
		/ js               ... all js files rendered in the finished page
		/ fonts            ... all fonts rendered in the finished page
		/ img              ... all images files rendered in the finished page

### Clean project setup with git submodules

1. Create a project directory and open a terminal there
2. Initialize a git repo

        git init

3. Checkout ADA as submodule

        git submodule add git@bitbucket.org:manuelmanhartit/ada.git

4. (optional) To update ADA to the latest version type (in your project dir)

        git submodule update --recursive --remote

### Configuration of ADA

ADA is very flexible yet also very easy to use. So you can start right away without the need to configure anything. While you advance with this framework you can use the configuration to ensure it still fits your needs.

By default the 'ada-config' looks like this (see in `.ada-config-default.json`):

    {
        "workspacePath": "./ada-files",
        "contentPath": "/content",
        "siteConfigFile": "site-config.json",
        "templateFile": "template.html",
        "contentFileParam": "f"
    }

You can easily override it by creating a `.ada-config.json` file in your project directory. You will need at least the values listed above but can define whatever additional configurations you like.

To load a custom value in PHP from the ada configuration just use:

    $value = $this->getAdaConfigValue('yourKey')

To load a custom value in PHP from the site configuration just use:

    $value = $this->getSiteConfigValue('yourKey');

Everything defined in the "siteConfigFile" will be available in the template file(s) and the content files.

### Creation of static html content pages

see Section above named TLDR;...

### Creation of dynamic php content pages

The easiest way to use ADA for creating dynamic pages is to create own templates in the 'ada-config' like this:

    "templates": {
        "before": "before.html",
        "after": "after.html"
    }

and the corresponding file in the workspace root. Then you can use them in your PHP files like this:

    <?php
    // include the Ada framework
    require_once('ada/framework.php');
    // actually run it
    $ada = new Ada();
    $ada->printPartialFromConfig('templates.before');

    // place your own php code here

    $ada->printPartialFromConfig('templates.after');
    ?>

For the code of the full example have a look at `examples/dynamic`.

### Mix static and dynamic content pages

You can of course have static and dynamic content pages together in one project.

For example code have a look at `examples/dynamic+static`.

## Prerequisites

* Any modern browser
* PHP development environment (if using docker eg. https://github.com/mikechernev/dockerised-php.git)

## Installing / Running

(optional)
If you use Sass for creating your css and want auto compiling the scss run

    # sass main.scss main.css --watch

Place the project directory in your webserver / php dev environment and call it (eg. http://127.0.0.1/).

## Built With / Resources

* [PHP 7.3.0](http://www.php.net/)
* [Twig 3.7.1](https://twig.symfony.com/)
* [strapi-php-client 1.0.0](https://twig.symfony.com/)
* https://docs.strapi.io/dev-docs/api/rest/populate-select#relation-media-fields
* https://docs.strapi.io/dev-docs/api/rest/interactive-query-builder
* https://en.wikibooks.org/wiki/PHP_Programming/Caching


## Contributing

No contributing planned.

## Versioning

This project uses a major and a minor version as versioning system. You can find it on top of this file.

Major version upgrades usually means breaking changes, minor version changes means bugfixes and (non breaking) feature extensions.

### Changelog

#### 3.0

* [BREAKING] big internal refactoring of framework.php (-> now Ada.php)
  * [BREAKING] moved functions into new classes for config loading / accessing , template processing, param and session handling
  * [BREAKING] renamed `framework.php` to `Ada.php`
  * [BREAKING] Public API `$ada->getSiteConfigValue($key)` replaced with `$ada->getConfig()->getSiteConfigValue($key)`
  * [BREAKING] Public API `$ada->getAdaConfigValue($key)` replaced with `$ada->getConfig()->getAdaConfigValue($key)`
  * [BREAKING] Public API `$ada->getTwigTemplateFile($templateFile, $context)` replaced with `$ada->getTemplateFile($templateFile, $context)`
  * [BREAKING] Public API `$ada->getTwigTemplate($templateContent, $context)` replaced with `$ada->getTemplateProcessor()->getTemplate($templateContent, $context)`
  * [BREAKING] Public API `$ada->getWorkspaceFilePath($fileName)` replaced with `$ada->getConfig()->getWorkspaceFilePath($fileName)`
  * [BREAKING] Public API `$ada->getContentFilePath($fileName)` replaced with `$ada->getConfig()->getContentFilePath($fileName)`
  * TBD list all functions which have been moved
* added a Debugger class
* added a connection to Strapi Headless CMS to fetch data from
* added a docker environment where ADA & your project runs

#### 2.4

* added contains to stringHelper
* added code comments
* added session & param helper functions to ada framework: startSessionIfNeeded(), getFromSessionOrParam($name), setInSession($name, $value), getFromParam($name)
* added template printing functions: printPartial($templateFile),
* added functions for saving and reading configurations / states: saveToFile($fileName, $arrayContent), readFromFile($fileName)
* framework is now accessible via $GLOBALS['ada']

#### 2.3 *** Breaking changes ***

* Renamed `{{workspacePath}}` to `{{workspaceUri}}` for all templates. Will be read from `ada-config`  `workspaceUri` or as fallback from `workspacePath`.
* Bugfix made pages with noindex fully accessible in menus etc.
* Extracted some helper functions in classes `fileHelper.php` and `stringHelper.php` so that these are accessible from anywhere

#### 2.2

* Bugfix fixed linking to external websites in menus

#### 2.1

* Made the `DEFAULT_FILE_PARAM` public available so websites can fetch the file param and build custom behaviour on it 

#### 2.0

* Initial version with external examples, supports static, dynamic & mixed projects / websites / webapps

## Authors

* **Manuel Manhart** - *Initial work*

## Special thanks

* Scott Arciszewski for his [Twig Loader](https://gist.github.com/sarciszewski/b6cd3776fbd20acaf26b)
* Mike Chernev for his [Dockerized PHP](https://github.com/mikechernev/dockerised-php.git)
* Jonathan Klughertz for his [Hexo Theme Alpha Dust](https://github.com/klugjo/hexo-theme-alpha-dust)

## License

This project is licensed under MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) for details.
