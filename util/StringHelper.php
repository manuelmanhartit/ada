<?php
/** 
 * These are my php string helper functions.
 */
class StringHelper {

	/**
	 * returns true if $haystack starts with $needle
	 */
	function startsWith($haystack, $needle) {
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	/**
	 * returns true if $haystack ends with $needle
	 */
	function endsWith($haystack, $needle) {
		if ($haystack == null || $needle == null) {
			return null;
		}
		return substr($haystack,-strlen($needle)) === $needle;
	}

	/**
	 * returns true if $haystack contains $needle
	 */
	function contains($haystack, $needle) {
		return (strpos($haystack, $needle) !== false);
	}

	/**
	 * returns the $string with the first occurence of $replaceFirst replaced with $replaceWith
	 * if $replaceFirst cannot be found, just returns $string
	 */
	function replaceFirst($replaceFirst, $replaceWith, $string) {
		$temp = strpos($string, $replaceFirst);
		if ($temp === false) {
			return $string;
		} else {
			return substr($string, 0, $temp) . $replaceWith.substr($string, $temp + strlen($replaceFirst));
		}
	}
}

?>
