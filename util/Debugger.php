<?php
require_once('./ada/util/StringHelper.php');

/** 
 * These is a helper class for easy debugging.
 */
class Debugger {
	private $isEnabled;
	private $className;
	
	function __construct($isEnabled, $className = '') {
        $this->isEnabled = $isEnabled;
        $this->className = $className;
	}

	function debug($var, $title='', $isEnabled = true) {
        if ($this->isEnabled && $isEnabled) {
            if ($this->className != '' | $title != '') {
                echo '<h4>' 
                    . $this->className  
                    . ' - '
                    . $title;
                echo '</h4><pre>';
                var_dump($var);
                echo '</pre>';
            }
        }
	}
}

?>
