<?php
require_once('./ada/util/StringHelper.php');

/** 
 * These are my php file helper functions.
 */
class FileHelper {
	private $stringHelper;
	
	function __construct() {
		$this->stringHelper = new StringHelper();
	}

	/**
	 * returns the contents of a file as string
	 */
	function getFileContents($fileWithPath) {
		$myfile = fopen($fileWithPath, "r") or die("Unable to open file '$fileWithPath'!");
		return fread($myfile, filesize($fileWithPath));
	}

	function loadJsonConfig($fileWithPath) {
		if (file_exists($fileWithPath)) {
			return json_decode($this->getFileContents($fileWithPath), true);
		} else {
			return false;
		}
	}

	function saveJsonConfig($fileWithPath, $object) {
		return file_put_contents($fileWithPath, json_encode($object));
	}

	/**
	 * concatenates multiple parts of a path to one
	 */
	function concatPath(...$path) {
		$temp = '';
		foreach($path as $p) {
			if ($temp !== '' && !$this->stringHelper->endsWith($temp, '/') 
					&& !$this->stringHelper->startsWith($p, '/')) {
				$temp .= '/';
			}
			$temp .= $p;
		}
		return $temp;
	}
	/**
	 * 
	 */
	function saveToFile($fileName, $arrayContent) {
		file_put_contents($fileName, join("\n", $arrayContent));
	}
	
	function readFromFile($fileName) {
		if (file_exists($fileName)) {
			$f = fopen($fileName, 'r');
			$result = array_filter(explode("\n", fread($f, filesize($fileName))));
			fclose($f);
			return $result;
		}
		return false;
	}

}

?>
