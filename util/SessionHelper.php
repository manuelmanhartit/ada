<?php
/** 
 * These are my php parameter and session helper functions.
 */
class ParamAndSessionHelper {

	/**
	 * starts a new session if no session is yet set
	 */
	function startSessionIfNeeded() {
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	}

	/**
	 * gets a value via its given name from the paramters (GET) or the session 
	 * or return false if the value is not set
	 */
	function getFromSessionOrParam($name) {
		$result = $this->getFromParam($name);
		if (!$result) {
			return $this->getFromSession($name);
		}
	}

	/**
	 * gets a value via its given name from the paramters (GET) or return false 
	 * if the value is not set
	 */
	function getFromParam($name) {
		if (isset($_GET[$name])) {
			return $_GET[$name];
		}
		return false;
	}

	/**
	 * gets a value via its given name from the session or return false if the 
	 * value is not set
	 */
	function getFromSession($name) {
		$this->startSessionIfNeeded();
		if (isset($_SESSION[$name])) {
			return $_SESSION[$name];
		}
		return false;
	}

	// this does not seem to work - have to debug why...
	/**
	 * stores a value with a given name into the session
	 */
	function setInSession($name, $value) {
		$this->startSessionIfNeeded();
		$_SESSION[$name] = $value;
	}
}

?>
