<?php
require_once('./ada/util/Debugger.php');
require_once('./ada/util/StringHelper.php');
require_once('./ada/util/FileHelper.php');

require_once('./ada/connectors/ConfigLoader.php');
require_once('./ada/connectors/twig/TwigTemplateProcessor.php');

/** 
 * This is my php website creation support tool ADA (Alpha Dust Advanced).
 * 
 * The first design was based on a hexo theme called Alpha Dust created by 
 * Jonathan Klughertz.
 * If you want to know how to use, please look into README.md or the different 
 * examples.
 * 
 * To know more about how the framework works internally, feel free to browse 
 * the code.
 * 
 * TODO check if we can split this up even further or if we should leave it like it is
 */
class Ada {
	private ConfigLoader $config;
	private Debugger $debugger;
	private FileHelper $fileHelper;
	private StringHelper $stringHelper;
	private TwigTemplateProcessor $templateProcessor;
	// seems that is not neccessary anymore
	public String $DEFAULT_FILE_PARAM = 'contentFileParam';

	function __construct() {
		$this->fileHelper = new FileHelper();
		$this->stringHelper = new StringHelper();
		$this->debugger = new Debugger(false, 'Ada');
		$this->config = new ConfigLoader();
		$this->templateProcessor = new TwigTemplateProcessor();
		$GLOBALS['ada'] = $this;
		$this->setCurrentUriInConfig();
	}

	function setCurrentUriInConfig() {
		$currentUri = $this->getFileNameFromRequestParam($this->DEFAULT_FILE_PARAM);
		$serverUri = $_SERVER['REQUEST_URI'];
		if ($currentUri == '' && $serverUri != "") {
			$currentUri = $serverUri;
		}
		$this->getConfig()->setCurrentUri($currentUri);
		$this->debugger->debug('setting currentUri into config: ' . $currentUri);
	}

	/**
	 * This method is used for printing static content.
	 * 
	 * It resolves either directContent (used for eg. robots.txt, sitemap.xml) 
	 * without any processing or resolves 'templates.static' from the
	 * configuration and prints it.
	 * 
	 * @see printPartialFromConfig($templateName) for printing partials or 
	 * 		dynamic content
	 */
	function printHtmlPage() {
		$this->setCurrentUriInConfig();

		if ($this->getConfig()->getCurrentUri() === '/sitemap.xml') {
			// TODO this should also run through a template
			echo $this->getConfig()->getSitemap();
		} else if ($this->getConfig()->getCurrentUri() === '/robots.txt') {
			echo $this->getConfig()->fetchContentFromHtmlFile($this->getConfig()->getContentFilePath($this->getConfig()->getCurrentUri()), $this->getConfig()->getContext());
			if ($this->getConfig()->getContextValue('directContent') !== null) {
				echo $this->getConfig()->getContextValue('directContent');
			} else {
				echo $this->printPartialFromConfig('templates.static');
			}
		} else {
			$context = $this->getConfig()->getContext();
			$this->printPartial('template.html', $context);
		}
	}

	function getFileNameFromRequestParam($paramKey) {
		$paramKey = $this->getConfig()->getAdaConfigValue('contentFileParam');
		if (array_key_exists($paramKey, $_GET)) {
			return $_GET[$paramKey];
		}
		return false;
	}

	/**
	 * Resolves the templateName from the configuration and
	 * prints the processed twig template.
	 */
	function printPartialFromConfig($templateName) {
		echo $this->printPartial($this->getConfig()->getAdaConfigValue($templateName));
	}

	/**
	 * Prints the processed twig template with the given file name.
	 */
	function printPartial($templateFile, $context) {
		$this->setCurrentUriInConfig();

		// resolve static content body if neccessary
		if ($this->getConfig()->getContextValue('bodyFile') !== false) {
			$_body = $this->getStaticContentBody();
			if ($_body !== false) {
				$context['body'] = $_body;
			}
		}
		echo $this->getTemplateProcessor()->getTemplateFile($templateFile, $context, $this->getConfig()->getAdaConfigValue('workspacePath'));
	}

	/**
	 * Reads and prints the filePath with the according header (content-type) 
	 * set to type.
	 */
	function printResource($filePath, $type) {
		header('Content-Type:'.$type);
		header('Content-Length: ' . filesize($filePath));
		// be careful with uncommenting that debug statement since it breaks 
		// the serving of the resource
		//$this->debugger->debug($filePath, 'loading file: ');
		readfile($filePath);
	}

	function getStaticContentBody() {
		if ($this->getConfig()->getContextValue('bodyFile') != null) {
			return $this->getTemplateProcessor()->fetchContentFromHtmlFile($this->getConfig()->getContextValue('bodyFile'), $this->getConfig()->getContext(), $this->getConfig()->getAdaConfigValue('workspacePath'));
		} else {
			return false;
		}
	}

	function getConfig() {
		return $this->config;
	}

	function getTemplateProcessor() {
		return $this->templateProcessor;
	}

	function getTemplateFile($templateFile, $context) {
		return $this->getTemplateProcessor()->getTemplateFile($templateFile, $context, $this->getConfig()->getAdaConfigValue('workspacePath'));
	}

}

?>
