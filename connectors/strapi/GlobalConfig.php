<?php

use MBVienasBaitas\Strapi\Client\Client;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Single\ShowRequest;

class GlobalConfig {
	private $globalConfig = null;
	private $url = null;
	private $token = null;
	private $debugger;

    public function __construct($url, $token) {
        $this->url = $url;
        $this->token = $token;
		$this->debugger = new Debugger(false, 'GlobalConfig');
    }

    public function getGlobalConfig() {
		$this->debugger = new Debugger(false, 'GlobalConfig');
        if ($this->globalConfig == null) {
            $this->globalConfig = $this->fetchGlobalConfig();
        }
        return $this->globalConfig;
    }

    public function fetchGlobalConfig() {
        $request = ShowRequest::make();
        $client = new Client($this->url, $this->token);
        $endpoint = $client->single('global-config?populate[footer][populate][footerMenu][fields][0]=*&populate[header][populate][headerMenu][fields][0]=*&publicationState=live');
        $response = $endpoint->show($request);
        $this->debugger->debug('Loading GlobalConfig', $response);
        if (isset($response) && $response != '') {
            return $response["data"]["attributes"];
        }
        return $response;
    }

    public function getSupportedLanguages() {
        // TODO fetch supported languages either from strapi or from global config
        return 'en';
    }

    public function getHeader() {
        $this->debugger->debug('GetHeader', $this->getGlobalConfig()['data']['attributes']['header']);
        return $this->getGlobalConfig()['data']['attributes']['header'];
    }
    
    public function getFooter() {
        $this->debugger->debug('GetFooterMenu', $this->getGlobalConfig()['data']['attributes']['footer']);
        return $this->getGlobalConfig()['data']['attributes']['footer'];
    }

    public function debug($title, $var) {
        echo '<h3>' . $title . '</h3>' ;
        echo '<pre>' ;
        var_dump($var);
        echo '</pre>';
    }

}

?>
