<?php

use MBVienasBaitas\Strapi\Client\Client;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Collection\ShowRequest;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionId;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionPopulateNested;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Collection\IndexRequest;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionLocale;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionSortAsc;

class Collections {
    function getList($url, $token, $locale, $collectionName, $sortBy = 'title') {
        $client = new Client($url, $token);
        $request = IndexRequest::make(
            new OptionLocale($locale),
            new OptionSortAsc($sortBy),
        );
        $client = new Client($url, $token);
        $endpoint = $client->collection($collectionName);
        return $endpoint->scroll($request);
    }    
    function get($url, $token, $collectionName, $id, $request = null) {
        if ($request == null) {
            $request = ShowRequest::make(
                new OptionId($id)
            );
        }
        $client = new Client($url, $token);
        $endpoint = $client->collection($collectionName);
        $r = $endpoint->show($request);
        //var_dump($r);
        return $r;
    }
}

?>
