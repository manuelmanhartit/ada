<?php

require_once './ada/connectors/strapi/Collections.php';

use MBVienasBaitas\Strapi\Client\Contracts\Requests\Collection\ShowRequest;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionId;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionPopulateNested;

class Pages {
	private $globalConfig = null;
	private $url = null;
	private $token = null;
	private $debugger;

    public function __construct($url, $token) {
        $this->url = $url;
        $this->token = $token;
		$this->debugger = new Debugger(false, 'Pages');
    }

    function getPages($locale) {
        $c = new Collections();
        $result =  $c->getList($this->url, $this->token, $locale, 'Pages', 'title');
        return $result;
    }
    
    function getPage($id) {
        $request = ShowRequest::make(
            new OptionId($id),
            new OptionPopulateNested(["contentSections"])
        );
        $c = new Collections();
        return $c->get($this->url, $this->token, 'Pages', $id, $request);
    }
}

?>
