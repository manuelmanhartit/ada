<?php

require __DIR__ . '/../vendor/autoload.php';

use MBVienasBaitas\Strapi\Client\Client;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Collection\ShowRequest;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionLocale;
use MBVienasBaitas\Strapi\Client\Contracts\Requests\Options\OptionSortAsc;

class CollectionDetailConnector {
    function showRequest($url, $token, $collectionName) {
        $endpoint = $client->collection($collectionName);
        return $endpoint->show($request) || die('Antwort fehlerhaft');
    }
    
    function get($url, $token, $locale, $collectionName, $sortBy = 'title') {
        $request = ShowRequest::make(
            new OptionId(3)
        );
        $client = new Client($url, $token);
        $endpoint = $client->collection($collectionName);
        return $endpoint->show($request);
    }    
}

?>
