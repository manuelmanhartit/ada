<?php
require_once('./ada/util/Debugger.php');
require_once('./ada/util/StringHelper.php');
require_once('./ada/connectors/strapi/GlobalConfig.php');
require_once('./ada/connectors/strapi/Pages.php');

/** 
 * This loads the configuration, first from some static files, and if configured, also from Strapi
 */
class StrapiConfig {
	private $adaConfig;
	private $siteConfig;
    private $context;
    private String $workspaceUrl;
    private String $securityToken;

	private Debugger $debugger;
	private StringHelper $stringHelper;
    private GlobalConfig $strapiGlobalConfig;
    private Pages $pages;

	function __construct($workspaceUrl, $securityToken) {
        $this->workspaceUrl = $workspaceUrl;
        $this->securityToken = $securityToken;
		$this->stringHelper = new StringHelper();
		$this->debugger = new Debugger(false, 'StrapiConfig');
        $this->debugger->debug($this->workspaceUrl . " - " . $this->securityToken, "Strapi url and token: ");
        $this->strapiGlobalConfig = new GlobalConfig($this->workspaceUrl, $this->securityToken);
        $this->pages = new Pages($this->workspaceUrl, $this->securityToken);
		$this->loadConfig();
	}

	function loadConfig() {
        $this->siteConfig = $this->strapiGlobalConfig->getGlobalConfig();
        $this->debugger->debug($this->siteConfig, 'Strapi Config');
        return $this->siteConfig;
	}

	function getPages($locale) {
        $pages = $this->pages->getPages($locale);
        $this->debugger->debug($pages, 'Pages');
        return $pages;
	}

	function buildContext($currentUri, $locale) {
		$context = $this->siteConfig;
        $pages = $this->getPages($locale);
        foreach ($pages as $item) {
            $data = $item['data'];
            $attr = $data['attributes'];
            $slug = $attr['slug'];
            $id = $data['id'];
            if ($slug == $currentUri) {
                $this->debugger->debug($item, 'CURRENT PAGE');
                $context['currentPage'] = $this->pages->getPage($id)['data']['attributes'];
            } else {
                $this->debugger->debug($item, 'pages');
            }
        }
        $this->debugger->debug($context, 'returning context');
        return $context;
    }

	function calculateLastModifiedForAllPages($context) {
		$pages = [];
		$lastModified = date('Y-m-d', strtotime('2019-01-01'));
		foreach ($context['pages'] as $page) {
			$href = $page['href'];
			if ( !in_array($href, $pages) ) {
				if ($this->stringHelper->startsWith($href, 'http')) {
					// nothing to do here but down below to push the page back into the array
					$page['lastModified'] = '';
				} else if ($this->stringHelper->endsWith($href, '.html')) {
					$page['lastModified'] = date('Y-m-d', filemtime($this->getContentFilePath($href)));
				} else {
					$page['lastModified'] = date('Y-m-d', filemtime($href));
				}
				if ($lastModified < $page['lastModified']) {
					$lastModified = $page['lastModified'];
				}
				array_push($pages, $page);
			}
		}
		$context['pages'] = $pages;
		$context['lastUpdated'] = $lastModified;
		return $context;
	}

	function findNavigationName($navigationArray, $hrefValue) {
		foreach ($navigationArray as $item) {
			if ($this->stringHelper->endsWith($hrefValue, $item['href'])) {
				return $item;
			}
		}
		return false;
	}

	/**
	 * Prints the sitemap based on the pages defined in the configuration
	 * TODO refactor since this should not be in the config...
	 */
	function getSitemap($context) {
		$pagesAdded = [];
		header("Content-type: text/xml");
		$result = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		$domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' 
			? 'https' 
			: 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		foreach ($context['pages'] as $page) {
			$href = $page['href'];
			if ( !in_array($href, $pagesAdded) 
					&& !$this->stringHelper->startsWith($href, 'http') 
					&& (!array_key_exists('noindex', $page) 
						|| $page['noindex'] === false)
			) {
				array_push($pagesAdded, $href);
				$result .= '<url><loc>'
					. $domain . $href
					. '</loc><lastmod>'
					. $page['lastModified']
					. '</lastmod></url>';
			}
		}
		$result .= '</urlset>';
		return $result;
	}
}

?>
