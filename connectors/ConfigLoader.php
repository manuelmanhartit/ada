<?php
require_once('./ada/util/Debugger.php');
require_once('./ada/util/StringHelper.php');
require_once('./ada/util/FileHelper.php');
require_once('./ada/connectors/StrapiConfig.php');
require_once('./ada/connectors/FileConfig.php');

/** 
 * This loads the configuration, first from some static files, and if configured, also from Strapi
 */
class ConfigLoader {
	private String $adaConfigFile = 'ada-config.json';
	private String $defaultAdaConfigFile = './ada/.ada-config-default.json';

	private $adaConfig;
	private $siteConfig;
	private $context;
	private String $currentUri;

	private Debugger $debugger;
	private FileHelper $fileHelper;
	private StringHelper $stringHelper;
	private StrapiConfig $strapiConfig;
	private FileConfig $fileConfig;
	
	function __construct() {
		$this->fileHelper = new FileHelper();
		$this->stringHelper = new StringHelper();
		$this->debugger = new Debugger(false, 'ConfigLoader');
		$this->fileConfig = new FileConfig();
		$this->adaConfig = $this->fileConfig->getAdaConfig();
        	$this->debugger->debug($this->getAdaConfigValue('workspaceType'), 'Workspace Type');
		$this->loadConfig();
	}

	function isFileWorkspaceType() {
		return $this->getWorkspaceType() == '' || $this->getWorkspaceType() == 'file';
	}

	function isStrapiWorkspaceType() {
		return $this->getWorkspaceType() == 'strapi';
	}

	function getWorkspaceType() {
		return $this->getAdaConfigValue('workspaceType');
	}

	function getCurrentUri() {
		if (!isset($this->currentUri)) {
			return null;
		}
		return $this->currentUri;
	}

	function setCurrentUri($uri) {
		$this->currentUri = $uri;
	}

	function getSupportedLocales() {
		// TODO implement me
		return [];
	}

	function getLocale() {
		// fetch locale from browser and match against supported locales
		$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		$acceptLang = $this->getSupportedLocales();
		$this->debugger->debug($lang, "current browser language");
		$lang = in_array($lang, $acceptLang) ? $lang : 'en';
		$this->debugger->debug($lang, "accepted language");
		return $lang;
	}

	function getPages() {
		$this->debugger->debug($this->fileConfig->getPages($this->getLocale()), "file pages");
		if ($this->isStrapiWorkspaceType()) {
			return $this->strapiConfig->getPages($this->getLocale());
		}
		return $this->fileConfig->getPages($this->getLocale());
	}
	//$context['currentPage'] = $this->findNavigationName( $pages, $_SERVER['REQUEST_URI'] );

	function loadConfig() {
        if ($this->isStrapiWorkspaceType()) {
			$this->strapiConfig = new StrapiConfig($this->getAdaConfigValue('workspaceUrl'), $this->getAdaConfigValue('token'));

			$this->siteConfig = $this->fileConfig->getSiteConfig();
			$_strapiConfig = $this->strapiConfig->loadConfig();
            if ($this->siteConfig != null) {
                $this->siteConfig = array_merge($this->siteConfig, $_strapiConfig);
                $this->debugger->debug($this->siteConfig, 'Merged Site Config');
            }
        }
	}

	function addContextValue($key, $value) {
		$_context = $this->getContext();
		$this->debugger->debug($key . ' - ' . $value, 'Setting key / value to context');
		$_context[$key] = $value;
		$this->context = $_context;
	}

	function getContextValue($key) {
		return $this->_getConfigValue($this->getContext(), $key);
	}

	function getSiteConfigValue($key) {
		return $this->_getConfigValue($this->siteConfig, $key);
	}

	function getAdaConfigValue($key) {
		return $this->_getConfigValue($this->adaConfig, $key);
	}

	function _getConfigValue($config, $key) {
		if ($config == null || $key == null) {
			return null;
		}
		// TODO implement fetching values from arrays (nested objects do work)
		if (strpos($key, '.') !== false) {
			$keys = explode('.', $key);
			foreach ($keys as $k) {
				if (array_key_exists($k, $config)) {
					$config = $config[$k]; // or die("Configuration '$key' does not exist, please check your ADA adaConfig file!"); // does not work
				} else {
					return null;
				}
			}
			return $config;
		} else if (array_key_exists($key, $config)) {
			return $config[$key];
		} else {
			return null;
		}
	}

	function getContext() {
		if ($this->context == null) {
			if ($this->isStrapiWorkspaceType()) {
				$context = $this->strapiConfig->buildContext($this->getCurrentUri(), $this->getLocale());
			} else if ($this->isFileWorkspaceType()) {
				$this->debugger->debug($this->getCurrentUri(), "config.currentUri");
				$context = $this->fileConfig->buildContext($this->getCurrentUri(), $this->getLocale());
			}
			$this->context = $context;
		}
		return $this->context;
	}

	function getContentFilePath($fileName) {
		return $this->getAdaConfigValue('workspacePath') . $this->getAdaConfigValue('contentPath') . '/' . $fileName;
	}

	function calculateLastModifiedForAllPages($context) {
		if ($this->isStrapiWorkspaceType()) {
			$context = $this->strapiConfig->calculateLastModifiedForAllPages($context);
		} else if ($this->isFileWorkspaceType()) {
			$context = $this->fileConfig->calculateLastModifiedForAllPages($context);
		}
		return $context;
	}

	function findNavigationName($navigationArray, $hrefValue) {
		foreach ($navigationArray as $item) {
			if ($this->stringHelper->endsWith($hrefValue, $item['href'])) {
				return $item;
			}
		}
		return false;
	}

	/**
	 * Prints the sitemap based on the pages defined in the configuration
	 * TODO refactor since this should not be in the config...
	 */
	function getSitemap($context) {
		$pagesAdded = [];
		header("Content-type: text/xml");
		$result = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		$domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' 
			? 'https' 
			: 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		foreach ($context['pages'] as $page) {
			$href = $page['href'];
			if ( !in_array($href, $pagesAdded) 
					&& !$this->stringHelper->startsWith($href, 'http') 
					&& (!array_key_exists('noindex', $page) 
						|| $page['noindex'] === false)
			) {
				array_push($pagesAdded, $href);
				$result .= '<url><loc>'
					. $domain . $href
					. '</loc><lastmod>'
					. $page['lastModified']
					. '</lastmod></url>';
			}
		}
		$result .= '</urlset>';
		return $result;
	}
}

?>
