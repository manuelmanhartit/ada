<?php

/**
 * Takes care of all twig processing
 */
class TwigTemplateProcessor {

	function __construct() {
	}

	function fetchContentFromHtmlFile($fileName, $context, $baseFilePath) {
		return $this->getTemplateFile($fileName, $context, $baseFilePath);
	}

	function addContextValue($key, $value) {
		$_context = $this->getContext();
		$_context[$key] = $value;
		$this->context = $_context;
	}

	function getContextValue($key) {
		return $this->_getConfigValue($this->getContext(), $key);
	}

	function getTemplateFile($templateFile, $context, $baseFilePath) {
		$loader = new \Twig\Loader\FilesystemLoader($baseFilePath);
		$twig = new \Twig\Environment($loader);
		return $twig->render($templateFile, $context);
	}

	function getTemplate($templateContent, $context) {
		$loader = new \Twig\Loader\ArrayLoader([
			'index.html' => $templateContent
		]);
		$twig = new \Twig\Environment($loader);
		return $twig->render('index.html', $context);
	}
}

?>
