<?php
require_once('./ada/util/Debugger.php');
require_once('./ada/util/StringHelper.php');
require_once('./ada/util/FileHelper.php');

/** 
 * This loads the configuration, first from some static files, and if configured, also from Strapi
 */
class FileConfig {
	private String $adaConfigFile = 'ada-config.json';
	private String $defaultAdaConfigFile = './ada/.ada-config-default.json';

	private $adaConfig;
	private $siteConfig;
    private $context;

	private Debugger $debugger;
	private FileHelper $fileHelper;
	private StringHelper $stringHelper;

	function __construct() {
		$this->fileHelper = new FileHelper();
		$this->stringHelper = new StringHelper();
		$this->debugger = new Debugger(false, 'FileConfig');
		$this->loadConfig();
	}

	function getAdaConfig() {
		return $this->adaConfig;
	}

	function getSiteConfig() {
		return $this->siteConfig;
	}

	function getPages($locale) {
		return $this->getSiteConfigValue('pages');
	}

	function loadConfig() {
        $this->adaConfig = $this->fileHelper->loadJsonConfig($this->defaultAdaConfigFile);
        $this->debugger->debug($this->adaConfig, 'ada default config: ');
		if (file_exists($this->adaConfigFile)) {
			$_adaConfigOverrides = $this->fileHelper->loadJsonConfig($this->adaConfigFile);
			$this->debugger->debug($_adaConfigOverrides, 'ada config override values: ');
			$this->adaConfig = array_merge($this->adaConfig, $_adaConfigOverrides);
			$this->debugger->debug($this->adaConfig, 'merged ada config: ');
		}
		$_siteConfigFile = $this->getWorkspaceFilePath($this->getAdaConfigValue('siteConfigFile'));
		if (file_exists($_siteConfigFile)) {
            $this->debugger->debug($_siteConfigFile, 'site config file path: ');
			$this->siteConfig = $this->fileHelper->loadJsonConfig($_siteConfigFile);
			$this->debugger->debug($this->siteConfig, 'site config: ');
		}
	}

	function getWorkspaceFilePath($fileName) {
		return $this->getAdaConfigValue('workspacePath') . '/' . $fileName;
	}

	function addContextValue($key, $value) {
		$_context = $this->getContext();
		$_context[$key] = $value;
		$this->context = $_context;
	}

	function getContextValue($key) {
		return $this->_getConfigValue($this->getContext(), $key);
	}

	function getSiteConfigValue($key) {
		return $this->_getConfigValue($this->siteConfig, $key);
	}

	function getAdaConfigValue($key) {
		return $this->_getConfigValue($this->adaConfig, $key);
	}

	function _getConfigValue($config, $key) {
		// TODO implement fetching values from arrays (nested objects do work)
		if (strpos($key, '.') !== false) {
			$keys = explode('.', $key);
			foreach ($keys as $k) {
				if (array_key_exists($k, $config)) {
					$config = $config[$k]; // or die("Configuration '$key' does not exist, please check your ADA adaConfig file!"); // does not work
				} else {
					return null;
				}
			}
			return $config;
		} else if (array_key_exists($key, $config)) {
			return $config[$key];
		} else {
			return null;
		}
	}

	function buildContext($currentUri, $locale) {
		$context = $this->siteConfig;
		if ($this->getAdaConfigValue('workspaceUri')) {
			$context['workspaceUri'] = $this->getAdaConfigValue('workspaceUri');
		} else {
			$this->debugger->debug($this->getAdaConfigValue('workspacePath'));
			$this->debugger->debug($context);
			$context['workspaceUri'] = $this->getAdaConfigValue('workspacePath');
		}
		$this->getAdaConfigValue('workspacePath');
		$pages = $context['pages'];
		$context = $this->calculateLastModifiedForAllPages($context);
		if ($currentUri === false) {
			$currentUri = $_SERVER['REQUEST_URI'];
		} //else {
			$this->debugger->debug($currentUri, 'current uri: ');
			// set current page from parameter or path
			$context['currentPage'] = $this->findNavigationName( $pages, $currentUri );
			if ($context['currentPage'] === false) {
				// set first entry in pages as default current page
				$context['currentPage'] = $pages[0];
			}
		//}
		if (array_key_exists('currentPage', $context) && $this->stringHelper->endsWith($context['currentPage']['href'], '.html')) {
			$context['bodyFile'] = $this->getContentFilePathInsideWorkspace($context['currentPage']['href']);
		}
		//var_dump($context);
		return $context;
	}

	function getContentFilePathInsideWorkspace($filePath) {
		return $this->getAdaConfigValue('contentPath') . '/' . $filePath;
	}

	function getContentFilePath($filePath) {
		return $this->getAdaConfigValue('workspacePath') . $this->getContentFilePathInsideWorkspace($filePath);
	}

	function calculateLastModifiedForAllPages($context) {
		$pages = [];
		$lastModified = date('Y-m-d', strtotime('2019-01-01'));
		foreach ($context['pages'] as $page) {
			$href = $page['href'];
			if ( !in_array($href, $pages) ) {
				if ($this->stringHelper->startsWith($href, 'http')) {
					// nothing to do here but down below to push the page back into the array
					$page['lastModified'] = '';
				} else if ($this->stringHelper->endsWith($href, '.html')) {
					$page['lastModified'] = date('Y-m-d', filemtime($this->getContentFilePath($href)));
				} else {
					$page['lastModified'] = date('Y-m-d', filemtime($href));
				}
				if ($lastModified < $page['lastModified']) {
					$lastModified = $page['lastModified'];
				}
				array_push($pages, $page);
			}
		}
		$context['pages'] = $pages;
		$context['lastUpdated'] = $lastModified;
		return $context;
	}

	function findNavigationName($navigationArray, $hrefValue) {
		foreach ($navigationArray as $item) {
			$this->debugger->debug($item, 'searching for nav item: ' . $hrefValue);
			if ($this->stringHelper->endsWith($hrefValue, $item['href'])) {
				return $item;
			}
		}
		return false;
	}

	/**
	 * Prints the sitemap based on the pages defined in the configuration
	 * TODO refactor since this should not be in the config...
	 */
	function getSitemap($context) {
		$pagesAdded = [];
		header("Content-type: text/xml");
		$result = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		$domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' 
			? 'https' 
			: 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		foreach ($context['pages'] as $page) {
			$href = $page['href'];
			if ( !in_array($href, $pagesAdded) 
					&& !$this->stringHelper->startsWith($href, 'http') 
					&& (!array_key_exists('noindex', $page) 
						|| $page['noindex'] === false)
			) {
				array_push($pagesAdded, $href);
				$result .= '<url><loc>'
					. $domain . $href
					. '</loc><lastmod>'
					. $page['lastModified']
					. '</lastmod></url>';
			}
		}
		$result .= '</urlset>';
		return $result;
	}
}

?>
